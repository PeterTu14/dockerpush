FROM busybox
MAINTAINER Peter Tutka
ENTRYPOINT ["/bin/cat"]
CMD ["/etc/passwd"]